# Eelzee CircleCI Docker Images

The files in this folder represent custom extensions of the CircleCI 
Docker convenience images, extended to include some custom packages
necessary for our builds.  This repository is meant to be directly
consumed by [Docker Hub](https://docs.docker.com/docker-hub/builds/).

These images will primarily be consumed by Eelzee projects running
our [CircleCI Build Scripts](https://bitbucket.org/eelzeedev/circleci/src/master/).
See more about example usage there.